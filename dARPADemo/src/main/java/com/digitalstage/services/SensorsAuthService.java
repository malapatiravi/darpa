package com.digitalstage.services;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import com.digitalstage.generalobjects.AndroidFileManager;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by malz on 11/21/16.
 */
public class SensorsAuthService extends Service {
    private String TAG="SensorAuthService";
    private String Username;
    private String DeviceFol;
    private String Filename;
    private String Folder;
    private String AuthorizedUser;
    private String TemplateFilename;
    private AndroidFileManager AFMObject;
    private File DataFile;
    private BufferedReader DataBuffer;
    private boolean FileOpened = false;
    private int NumberOfRequiredLines=500;
    private boolean KeepLooping = true;
    private long LastFileSize=0;
    private boolean IsAuthenticated=false;
    private boolean FileLocked = false;
    private String[] Message;
    private File root;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message=new String[10];
        Toast.makeText(getApplicationContext(), "Gait Authentication Started", Toast.LENGTH_SHORT).show();
        Username=intent.getExtras().getString("Username");
        DeviceFol=intent.getExtras().getString("DeciveFol");
        Filename=intent.getExtras().getString("Filename");
        Folder=intent.getExtras().getString("Folder");
        AuthorizedUser=intent.getExtras().getString("AuthorizedUser");
        TemplateFilename=intent.getExtras().getString("TemplateFilename");
        root=new File(Environment.getExternalStorageDirectory(),Folder);
        //AFMObject=new AndroidFileManager()
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
