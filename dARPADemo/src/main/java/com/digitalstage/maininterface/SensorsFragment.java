package com.digitalstage.maininterface;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.graphics.AvoidXfermode;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalstage.authentication.SensorsTemplateManager;
import com.digitalstage.authentication.SensorsTemplateManagerR;
import com.digitalstage.authentication.WekaClassifierRemoval;
import com.digitalstage.darpademo.R;

import com.digitalstage.generalobjects.AndroidFileManager;
import com.digitalstage.services.SensorsClass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import weka.core.Check;

/**
 * Created by malz on 11/19/16.
 */
public class SensorsFragment extends Fragment {
    private Button AuthBtn;
    private String TAG="SensorFrag";
    private Intent SensorsService;
    private Intent SensorAuthService;
    private RadioGroup DemoMode;
    private EditText UserID;
    List<String> DataFromMain;

    private int NumberOfFeatures=23;
    private String CurrentFolder="";
    private String CurrentFile="";
    private String Folder="DataFolder";
    private String CurrentMode="Training";
    private String AuthorizedUser="Trainer";
    private String TestingFilename="Testing.txt";
    private String TemplateFilename="Training.arff";
    //The below SensorFol define the folder in which the data to be stored.
    private String SensorFol="Sensors";
    private MainActivity CurrentActivity;
    private String UserIDVal="NA";
    private TextView UpdateMsgBox;
    private TextView TotalLines;
    private int NumberOfRequiredLine=1000;
    private String currModeVal="";
    private boolean StopSensors=false;
    private String curr_file_path;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CurrentActivity=(MainActivity)getActivity();
        final View rootView=inflater.inflate(R.layout.sensors, container, false);
        AuthBtn=(Button)rootView.findViewById(R.id.AuthNotif);
        AuthBtn.setBackgroundColor(Color.RED);
        UpdateMsgBox=(TextView)rootView.findViewById(R.id.UpdateMessageBox);
        TotalLines=(TextView)rootView.findViewById(R.id.TotalLines);
        TotalLines.setText("0 Lines");
        Button StartBtn=(Button)rootView.findViewById(R.id.StartButton);
        Button StopBtn=(Button)rootView.findViewById(R.id.StopButton);
        SensorsService=new Intent(getActivity(), SensorsClass.class);

        StartBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                UpdateMsgBox.setText("Collecting Training Data... Please wait...\n");
                TotalLines.setText("Empty");
                UserID=(EditText)rootView.findViewById(R.id.UserID);
                if(UserID.length()<4)
                {
                    Toast.makeText(getActivity().getBaseContext(),"User ID can not be empty",Toast.LENGTH_SHORT).show();

                }
                else
                {
                    UserIDVal=UserID.getText().toString();
                    //Log.i(TAG,"User id = "+UserIDVal);

                    DemoMode=(RadioGroup)rootView.findViewById(R.id.DemoMode);
                    int SelectedMode = DemoMode.getCheckedRadioButtonId();
                    RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectedMode);
                    String ModeVal = ModeRVal.getText().toString();
                    currModeVal=ModeVal;
                    SensorsService.putExtra("DemoMode",ModeVal);
                    SensorsService.putExtra("SensorFol",SensorFol);
                    SensorsService.putExtra("UserID",UserIDVal);
                    AuthorizedUser=UserIDVal;
                    SensorsService.putExtra("Folder",Folder);
                    CurrentMode= ModeVal;
                    curr_file_path=Folder+"/"+UserIDVal+"/"+SensorFol+"/";
                    //Starting the sensor service now while starting the sensor service we make the auth button red
                    if(getActivity().startService(SensorsService)==null)
                    {

                        Log.i(TAG,"No Service is running at the moment.");
                        Toast.makeText(getActivity().getBaseContext(),"No BackGround Service is running",Toast.LENGTH_SHORT).show();
                    }
                  /*  else
                    {
                        if(!CheckStopMessageThread.isAlive())
                        {
                            CheckStopMessageThread.start();
                        }
                    }*/
                }
            }
        });

        StopBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try
                {
                    StopLogging(curr_file_path);
                    UpdateMsgBox.setText("Training started and ended");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        return rootView;
    }

    /*
     * This thread checks if the stop command is issues or not and starts collecting the data.
     */
    Thread CheckStopMessageThread=new Thread()
    {
        @Override
        public void run() {
            boolean Temp=CurrentActivity.GotStopCommand();
            if(StopSensors != Temp)
            {
                Log.i(TAG,"Value is "+(CurrentActivity.GotStopCommand())+"and activity is: "+CurrentActivity.toString());
                try
                {
                    //StopLogging();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                StopSensors=false;
            }

        }
    };
    /*
     * The below function just stops the sensor logging service
     * further the function can be extended to stop various services independently.
     */
    private void StopLogging(String path) throws Exception
    {
        CurrentActivity.stopService(SensorsService);
        AuthBtn.setBackgroundColor(Color.RED);
        if(currModeVal.equals("TrainingGenu"))
        {
            doTrainingGenu(path);
        }
        else if(currModeVal.equals("TrainingImp"))
        {
            doTrainingImp(path);
        }
        else if(currModeVal.equals("Testing"))
        {
            doTesting(path);
        }
        //AuthBtn.setText("Authenticated"+currModeVal);
        //CurrentActivity.stopService(SensorAuthService);
        Log.i(TAG,"Stopping the sensor service");

    }
    private void doTesting(String path)
    {

        Log.i("Testing","Testing in the main"+path+"TestingAcc.txt");

        AndroidFileManager TrainingFile=new AndroidFileManager(path,CurrentMode+"Acc.txt");
        try
        {
            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 3);
            TrainingObject.createArffFileForTesting();
            File trainingFile=new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingAcc.arff");
            File testingFile=new File(Environment.getExternalStorageDirectory()+"/"+path,"TestingAcc.arff");
            String new_path=Environment.getExternalStorageDirectory()+"/"+path;
            WekaClassifierRemoval decision=new WekaClassifierRemoval(trainingFile, testingFile, TrainingFile.GetCurrentFolder());
            int FeatureIndexesToBeRemoved[]={4,18,32,46};
            decision.setFeatureIndexesToBeRemoved(FeatureIndexesToBeRemoved);
            decision.setThreshold(0.80);
            boolean Decision=decision.getFinalDecision();
            if(Decision==true)
            {
                AuthBtn.setText("Authenticated and True");
                AuthBtn.setBackgroundColor(Color.GREEN);
            }
            else{
                AuthBtn.setText("Rejected and False");
                AuthBtn.setBackgroundColor(Color.RED);
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private void doTrainingGenu(String path)
    {

        Log.i("Training","Training in the main"+path+"TrainingAcc.txt");
        AndroidFileManager TrainingFile=new AndroidFileManager(path,CurrentMode+"Acc.txt");
        try
        {
            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 1);
            TrainingObject.createArffFileForTraining("Genuine");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }



    }
    private void doTrainingImp(String path)
    {
        Log.i("Training","Training in the main"+path+"TrainingAcc.txt");
        AndroidFileManager TrainingFile=new AndroidFileManager(path,CurrentMode+"Acc.txt");
        try
        {
            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 2);
            TrainingObject.createArffFileForTraining("Imposter");
            combineImpAndGenu(path);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    private void combineImpAndGenu(String path)
    {
        Log.i("Combining:",Environment.getExternalStorageDirectory()+"/"+path+CurrentMode+"Acc.txt");
        //Combinig the Imposter and Genuine Files into Genuine File
        String imposterName = path+"TrainingImpAcc.txt";
        try
        {
            BufferedReader dataImposterFile=new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingImpAcc.arff")));
            BufferedReader dataGenuineFile=new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingGenuAcc.arff")));
            BufferedWriter dataTrainingFile=new BufferedWriter(new FileWriter(new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingAcc.arff")));
            //First read the Genuine File and write
            String aLine;
            while((aLine=dataGenuineFile.readLine())!=null)
            {
               dataTrainingFile.write(aLine);
                dataTrainingFile.newLine();
            }
            dataGenuineFile.close();
            boolean shallRead=false;
            while((aLine=dataImposterFile.readLine())!=null)
            {
                if(aLine.contains("@data"))
                {
                    shallRead=true;
                }
                if(!aLine.contains("@data")&&shallRead==true)
                {
                    dataTrainingFile.write(aLine);
                    dataTrainingFile.newLine();
                }

            }
            dataImposterFile.close();
            dataTrainingFile.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }



    }
}
