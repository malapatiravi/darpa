package com.digitalstage.maininterface;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;

import com.digitalstage.darpademo.R;

import java.util.List;

/**
 * Created by malz on 11/19/16.
 */

public class MainActivity extends FragmentActivity implements	ActionBar.TabListener 
{
	private String TAG="MainActivity";
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private boolean StopSwipeService=false;
	private String[] tabs={"NAO","Lego","Sensor", "EEG"};
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		//Initialization of View Pager and Actionbar of the view pager
		viewPager=(ViewPager)findViewById(R.id.pager);
		actionBar=getActionBar();
		mAdapter=new TabsPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Log.i("Initilization done","Initilaizing now");
		//Adding Tabs for the View Pager Adapter
		for(String tab_name:tabs)
			actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));

		/*
		 * Swipe the view pager and the list of tabs are scrolled
		 */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
				Log.i(TAG, "Current position: "+tabs[position]);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {
		viewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {
				//Do nothing if the same tabe is reselcted

	}
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
				//Do nothing if the tab is unselected
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//The below implementation is for back and home keys in android when any of the keys are pressed the data collection is stopped immediately.
		switch(keyCode)
		{
			case KeyEvent.KEYCODE_BACK:
				StopSwipeService=true;
				return true;

			case KeyEvent.KEYCODE_HOME:
				StopSwipeService=true;
				return true;
		}


		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

	}
	public boolean GotStopCommand()
	{
		if(StopSwipeService==true)
		{
			StopSwipeService=false;
			return true;
		}
		else
			return false;
	}
}
