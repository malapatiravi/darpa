package com.digitalstage.maininterface;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

/**
 * Created by malz on 11/19/16.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			Log.i("Returning","NAO");
			return new NAOFragment();
		case 1:
			Log.i("Returning","Lego");
			return new LegoFragment();
		case 2:
			Log.i("Returning","Sensor");
			return new SensorsFragment();
		case 3:
			Log.i("Returning"," EEG");
			return new EEGFragment();

		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 4;
	}

}
