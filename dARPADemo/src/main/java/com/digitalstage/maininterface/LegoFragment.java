package com.digitalstage.maininterface;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalstage.darpademo.R;

/**
 * Created by malz on 11/19/16.
 */
public class LegoFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.lego, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
