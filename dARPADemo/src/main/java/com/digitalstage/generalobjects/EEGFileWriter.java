package com.digitalstage.generalobjects;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by malz on 11/25/16.
 */
public class EEGFileWriter {
    String path;
    BufferedWriter dataWriter;
    File dataFile;
    public EEGFileWriter(String _path)
    {
        path=_path;
        Initialize();
    }
    public void Initialize()
    {
        dataFile=new File(path);
        try
        {
            dataWriter=new BufferedWriter(new FileWriter(dataFile));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }



    }
    public void eraseFile()
    {
        try
        {
            dataWriter=new BufferedWriter(new FileWriter(dataFile));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public void writeLine(String aLine)
    {
        try
        {
            dataWriter.write(aLine);
            dataWriter.newLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public void writeLineImposter(String aLine)
    {
        aLine=aLine+",Imposter";
        try
        {
            dataWriter.write(aLine);
            dataWriter.newLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void writeLineGenuine(String aLine)
    {
        aLine=aLine+",Genuine";
        try
        {
            dataWriter.write(aLine);
            dataWriter.newLine();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void closeFile()
    {
        try
        {
            dataWriter.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public String getPathwithOutFileName(String _path)
    {
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
    public  String getPathwithOutFileName()
    {
        String _path=path;
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
}
