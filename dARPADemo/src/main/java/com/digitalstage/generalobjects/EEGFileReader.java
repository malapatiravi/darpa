package com.digitalstage.generalobjects;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by malz on 11/25/16.
 */
public class EEGFileReader {
    String path;
    BufferedReader dataReader;
    File dataFile;
    public EEGFileReader(String _path)
    {
        path=_path;
        Initialize();
    }
    public void Initialize()
    {
        try
        {
            dataFile=new File(path,"");
            dataReader=new BufferedReader(new FileReader(dataFile));
            //Log.i("File Path is",dataFile.getName()+"/"+dataFile.getPath()+"/"+dataFile.getCanonicalFile());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }
    public void removeWasteData()
    {

    }
    public String getFileName()
    {
        return dataFile.getName();
    }
    public String getPathwithOutFileName(String _path)
    {
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
    public  String getPathwithOutFileName()
    {
        String _path=path;
        String result="";
        int count=0;
        int i=_path.length()-1;
        while(_path.charAt(i)!='/')
        {
            i--;
            count++;
        }
        return _path.substring(0,_path.length()-count);
    }
    public String getNextLine()
    {
        try
        {
            String aLine;

            if((aLine=dataReader.readLine())!=null)
            {
                return aLine;
            }
            else
                return "null";
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "null";
        }

    }
    public void closeFile()
    {
        try
        {
            dataReader.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
